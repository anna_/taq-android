package gr.tessera.taq.screens;


import android.app.ProgressDialog;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import butterknife.Bind;
import butterknife.ButterKnife;
import gr.tessera.taq.R;

public class WebViewActivity extends AppCompatActivity {

    @Bind(R.id.webView)
    WebView webView;
    private ProgressDialog progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_web_view);
        ButterKnife.bind(this);

        webView.getSettings().setJavaScriptEnabled(true);
        String url = getIntent().getStringExtra("URL");

        progressBar = ProgressDialog.show(WebViewActivity.this, getString(R.string.taq), getString(R.string.loading));

        webView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            public void onPageFinished(WebView view, String url) {
                if (progressBar.isShowing()) {
                    progressBar.dismiss();
                }
            }


        });
        webView.loadUrl(url);

    }

    /*
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    */
}






